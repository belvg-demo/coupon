<?php

namespace BelVG\DemoCoupon\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SpecificItem
 * @package BelVG\DemoCoupon\Console
 */
class SpecificItem extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\SalesRule\Model\RuleFactory
     */
    protected $ruleFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\CollectionFactory
     */
    protected $groupCollectionFactory;
    /**
     * @var \Magento\SalesRule\Model\Rule\Condition\ProductFactory
     */
    protected $productConditionFactory;
    /**
     * @var \Magento\SalesRule\Model\Rule\Condition\Product\FoundFactory
     */
    protected $foundFactory;

    /**
     * SpecificItem constructor.
     * @param \Magento\SalesRule\Model\RuleFactory $ruleFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
     * @param \Magento\SalesRule\Model\Rule\Condition\Product\FoundFactory $foundFactory
     * @param \Magento\SalesRule\Model\Rule\Condition\ProductFactory $productConditionFactory
     */
    public function __construct(
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory,
        \Magento\SalesRule\Model\Rule\Condition\Product\FoundFactory $foundFactory,
        \Magento\SalesRule\Model\Rule\Condition\ProductFactory $productConditionFactory
    )
    {
        $this->ruleFactory = $ruleFactory;
        $this->storeManager = $storeManager;
        $this->groupCollectionFactory = $groupCollectionFactory;
        $this->foundFactory = $foundFactory;
        $this->productConditionFactory = $productConditionFactory;
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Magento\Customer\Model\ResourceModel\Group\Collection $groups */
        $groups = $this->groupCollectionFactory->create();
        $customerGroupIds = $groups->getColumnValues('customer_group_id');
        $websitesIds = [];
        foreach ($this->storeManager->getWebsites() as $website) {
            $websitesIds[] = $website->getId();
        }
        $couponData = [
            'name' => 'Specific Item Coupon',
            'description' => '',
            'is_active' => true,
            'simple_action' => 'by_percent',
            'discount_amount' => 10,
            'simple_free_shipping' => 0,
            'coupon_type' => 2,
            'customer_group_ids' => $customerGroupIds,
            'coupon_code' => 'SPECIFICSKU',
            'website_ids' => $websitesIds,
        ];
        /** @var \Magento\SalesRule\Model\Rule $rule */
        $rule = $this->ruleFactory->create();
        $rule->setData($couponData);
        $sku = 'abcd';
        /** @var \Magento\SalesRule\Model\Rule\Condition\Product\Found $condition */
        $condition = $this->foundFactory->create()
            ->setType('Magento\SalesRule\Model\Rule\Condition\Product\Found')
            ->setValue(1)// 1 == FOUND
            ->setAggregator('all'); // match ALL conditions
        $rule->getConditions()->addCondition($condition);
        $subConditions = $this->productConditionFactory->create();
        $subConditions->setType('Magento\SalesRule\Model\Rule\Condition\Product');
        $subConditions->setAttribute('sku');
        $subConditions->setOperator('==');
        $subConditions->setValue($sku);
        $condition->addCondition($subConditions);
        $rule->save();

    }

    protected function configure()
    {
        $this->setName('belvg:demo:coupon:specific-item')
            ->setDescription('Generate Specific Item Coupon');
        parent::configure();
    }
}
