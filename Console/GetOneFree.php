<?php

namespace BelVG\DemoCoupon\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 */
class GetOneFree extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\SalesRule\Model\RuleFactory
     */
    protected $ruleFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\CollectionFactory
     */
    protected $groupCollectionFactory;

    /**
     * GetOneFree constructor.
     * @param \Magento\SalesRule\Model\RuleFactory $ruleFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
     */
    public function __construct(
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
    )
    {
        $this->ruleFactory = $ruleFactory;
        $this->storeManager = $storeManager;
        $this->groupCollectionFactory = $groupCollectionFactory;
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Magento\Customer\Model\ResourceModel\Group\Collection $groups */
        $groups = $this->groupCollectionFactory->create();
        $customerGroupIds = $groups->getColumnValues('customer_group_id');
        $websitesIds = [];
        foreach ($this->storeManager->getWebsites() as $website) {
            $websitesIds[] = $website->getId();
        }
        $couponData = [
            'name' => 'Buy one get one free Coupon',
            'description' => '',
            'is_active' => true,
            'simple_action' => 'buy_x_get_y',
            'discount_amount' => 1,
            'discount_step' => 1,
            'simple_free_shipping' => 0,
            'coupon_type' => 2,
            'customer_group_ids' => $customerGroupIds,
            'coupon_code' => 'BUYONE',
            'website_ids' => $websitesIds,
        ];

        /** @var \Magento\SalesRule\Model\Rule $rule */
        $rule = $this->ruleFactory->create();
        $rule->setData($couponData);
        $rule->save();
    }

    protected function configure()
    {
        $this->setName('belvg:demo:coupon:buy-x-get-y')
            ->setDescription('Generate Buy one get one free Coupon');
        parent::configure();
    }
}
