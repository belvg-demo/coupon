<?php

namespace BelVG\DemoCoupon\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FreeShipping
 * @package BelVG\DemoCoupon\Console
 */
class FreeShipping extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Magento\SalesRule\Model\RuleFactory
     */
    protected $ruleFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\CollectionFactory
     */
    protected $groupCollectionFactory;

    /**
     * FreeShipping constructor.
     * @param \Magento\SalesRule\Model\RuleFactory $ruleFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
     */
    public function __construct(
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
    )
    {
        $this->ruleFactory = $ruleFactory;
        $this->storeManager = $storeManager;
        $this->groupCollectionFactory = $groupCollectionFactory;
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Magento\Customer\Model\ResourceModel\Group\Collection $groups */
        $groups = $this->groupCollectionFactory->create();
        $customerGroupIds = $groups->getColumnValues('customer_group_id');
        $websitesIds = [];
        foreach ($this->storeManager->getWebsites() as $website) {
            $websitesIds[] = $website->getId();
        }
        $couponData = [
            'name' => 'Free shipping Coupon',
            'description' => '',
            'is_active' => true,
            'simple_action' => 'by_percent',
            'discount_amount' => 0,
            'simple_free_shipping' => 2,
            'coupon_type' => 2,
            'customer_group_ids' => $customerGroupIds,
            'coupon_code' => 'FREESHIPPING',
            'website_ids' => $websitesIds,
        ];
        /** @var \Magento\SalesRule\Model\Rule $rule */
        $rule = $this->ruleFactory->create();
        $rule->setData($couponData);
        $rule->save();
    }

    protected function configure()
    {
        $this->setName('belvg:demo:coupon:free')
            ->setDescription('Generate Free Shipping Coupon');
        parent::configure();
    }
}
